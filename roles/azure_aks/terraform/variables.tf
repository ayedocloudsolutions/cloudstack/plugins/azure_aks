variable "client_id" {
  description = "Azure Kubernetes Service Cluster service principal"
}

variable "client_secret" {
  description = "Azure Kubernetes Service Cluster service principal password"
}

variable "tenant_id" {
  description = "Azure Kubernetes Service Cluster tenant id"
}

variable "subscription_id" {
  description = "Azure Kubernetes Service Cluster subscription id"
}

variable "stack" {}

variable "resource_group" {}

variable "node_count" {
  default = 3
}

variable "node_type" {
  default = "Standard_D2_v2"
}

variable "location" {
  default = "Germany West Central"
}

variable "kubeconfig_file" {
}
variable "custom_tags" {
}
