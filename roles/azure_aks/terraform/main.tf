provider "azurerm" {
  features {}

  tenant_id       = var.tenant_id
  subscription_id = var.subscription_id
  client_id       = var.client_id
  client_secret   = var.client_secret
}

# Shared tags
locals {
  # Common tags to be assigned to all resources
  common_tags = {
    "cloudstack"  = var.stack
    "environment" = "cloudstack"
  }

  custom_tags = yamldecode(var.custom_tags)

  all_tags = merge(local.common_tags, local.custom_tags)
}

resource "azurerm_resource_group" "default" {
  name     = var.resource_group
  location = var.location

  tags = {
    environment = "cloudstack"
  }
}

resource "azurerm_kubernetes_cluster" "default" {
  name                = join("-", [var.stack, "aks"])
  location            = azurerm_resource_group.default.location
  resource_group_name = azurerm_resource_group.default.name
  dns_prefix          = join("-", [var.stack, "k8s"])

  default_node_pool {
    name            = "default"
    node_count      = var.node_count
    vm_size         = var.node_type
    os_disk_size_gb = 30
  }

  service_principal {
    client_id     = var.client_id
    client_secret = var.client_secret
  }

  addon_profile {
    oms_agent {
      enabled = false
    }
  }

  role_based_access_control {
    enabled = true
  }

  tags = {
    stack = var.stack
  }
}

resource "local_file" "kubeconfig" {
  content  = azurerm_kubernetes_cluster.default.kube_config_raw
  filename = var.kubeconfig_file
}
